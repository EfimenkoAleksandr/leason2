//
//  ViewController.m
//  RGB
//
//  Created by mac on 20.03.2018.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UISwitch *swich1;
@property (weak, nonatomic) IBOutlet UILabel *viewRGB;
@property (weak, nonatomic) IBOutlet UISlider *sliderR;
@property (weak, nonatomic) IBOutlet UISlider *sliderG;
@property (weak, nonatomic) IBOutlet UISlider *sliderB;
//@property(nonatomic) int r,g,b;

@end

@implementation ViewController
- (IBAction)swich1:(UISwitch *)sender {
    
    [self.image1 setHidden:self.swich1.isOn];
}

- (IBAction)sliderR:(UISlider *)sender {
    int value=self.sliderR.value;
    self.viewRGB.text=[NSString stringWithFormat:@"%d",value];
    //self.sliderR.value/=255;
    [self collorView];
}
- (IBAction)sliderG:(UISlider *)sender {
    int value=self.sliderG.value;
    self.viewRGB.text=[NSString stringWithFormat:@"%d",value];
    
    [self collorView];
}
- (IBAction)sliderB:(UISlider *)sender {
    int value=self.sliderB.value;
    self.viewRGB.text=[NSString stringWithFormat:@"%d",value];
   
    [self collorView];
}
-(void) collorView{
    self.view.backgroundColor = [[UIColor alloc] initWithRed:(self.sliderR.value)/255 green:(self.sliderG.value)/255 blue:(self.sliderB.value)/255 alpha:1];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
